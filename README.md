# Kubernetes Example

This project shows how to build, deploy, run and scale a simple micro-service with Docker and Kubernetes.
<p/>
Let's imagine we would have to build a common service like IAM (Identity and Access Management).
<p/>
The IAM has to run under <b>high avaiability</b> and <b>scalability</b> environment.
In this example, our IAM has the following services:

- User Management
- Policy Management
- Monitoring and Audit Manamgement
- Directory (LDAP/AD) Integration
- SSO (Single Sign On)
- MFA (Multi-Factor Authentication)

  


<h3>Pre-requisites<h3/>

Docker and Kubernetes need to be installed. 
See: 
https://docs.docker.com/install/ 
https://kubernetes.io/docs/tasks/tools/install-minikube/

<h3>Run the project<h3/>

Build the project:
`mvn clean install`

Start Kubernetes:

`minikube start --memory 4096 --cpus 3`
<p/>(cpus = Number of CPUs allocated to the minikube VM. Default is 2)
<p/>

Build the IAM Services image for Minikube - from this directory run the following:


`eval $(minikube docker-env)`

`docker build . -t iamservices`

Deploy the IAM Services:

`kubectl create -f iam-services.yml`

Display the Services currently running:

`open http://$(minikube ip):30080 `

`open http://$(minikube ip):30081 `

`open http://$(minikube ip):30082 `

`open http://$(minikube ip):30083 `

`open http://$(minikube ip):30084 `

`open http://$(minikube ip):30085 `

List the IAM Services currently up:

`kubectl get pods`

<h3>Scale Up</h3>
To create more instances of each Service do:
 
`minikube dashboard `

and then scale the number of instances

<h3>Failover</h3>
If a service fails, another is started automatically:
Choose the host of one the Services opened in your browser then kill it:

`kubectl delete pod <host>`

Refresh the browser (it will take a little while) and see that another pod is already there

<H3>Services Upgrades</h3>
Change the version in the IAM Services Controller class to 0.0.2. then type:

`docker build . -t iamservices:0.0.2`

Then open iam-services.yml and find-replace all the "latest" with "0.0.2". Save the changes and do:

`kubectl apply -f iam-services.yml --record`

then refresh the browser 

<h3>Services Rollback</h3>

Display the history of what was deployed do:
`kubectl rollout history deployment <deployment_name> `

Do a rollback: 
`kubectl rollout undo deployment <deployment_name> --to-revision=1`

<h3>Destroy the entire IAM Services</h3>
`kubectl delete -f iam-services.yml`

<h3>Stop Kubernetes</h3>
`minikube stop`