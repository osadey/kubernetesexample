package com.offersadey.kubernetesexample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
public class IAMServicesController {

    private final String version = "0.0.1";

    private IAMServices iamServices;

    @Value("${spring.application.name}")
    private String serviceName;

    public IAMServicesController(IAMServices iamServices) {

        this.iamServices = iamServices;
    }

    @RequestMapping(method = GET)
    @ResponseBody
    public String iamServices() throws UnknownHostException {

        String stringBuilder = "IAM Service: " + serviceName + " - " +
                iamServices.getIAMServices(serviceName) + "<br/>" +
                "Version: " + version + "<br/><br/>" +
                "Host: " + InetAddress.getLocalHost().getHostName() + "<br/>" +
                "IP: " + InetAddress.getLocalHost().getHostAddress() + "<br/>";
        return stringBuilder;
    }

}
