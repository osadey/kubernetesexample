package com.offersadey.kubernetesexample;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class IAMServices {

    private Map<String,String> mapIAMServices = new HashMap<>();

    public IAMServices() {

        mapIAMServices.put("user-management-service", "IAM: User Management Service");
        mapIAMServices.put("policy-management-service", "IAM: Policy Management Service");
        mapIAMServices.put("monitoring-audit-service", "IAM: Monitoring & Audit Service");
        mapIAMServices.put("ldap-service", "IAM: LDAP & Active Directory Service");
        mapIAMServices.put("sso-service", "IAM: SSO Service");
        mapIAMServices.put("mfa-service", "IAM: Multi-Factor Authentication Service");
    }

    String getIAMServices(String name) {

        return mapIAMServices.get(name);

    }

}
